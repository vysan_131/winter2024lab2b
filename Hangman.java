import java.util.Scanner;
public class Hangman{
	
	public static int isLetterInWord(String word, char c){ //return position of char
		
		for(int i = 0; i < 4; i++){
			
			if(word.charAt(i) == c){
				return i;
			}	
		}
		return -1;
		
	}	
	public static char toUpperCase(char c){ //being uppercase
		//String BiggerFont = Character.toString(c).toUpperCase();
		//return Character.toString(BiggerFont);
		return Character.toUpperCase(c);
		
		//String x = "hi"
		//x = x.toUpperCase();
		
		//char c = 'c'
		//c = Character.toUpperCase(c);
	}	
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){ //check each letter
		
		String updateWord = ""; //update 
		
		if(letter0){ 
			updateWord += word.charAt(0); //makes changes
		}
		else{ //if it's false
			updateWord += "_ "; //blank
		}	
		
		if(letter1){
			updateWord += word.charAt(1);
		}
		else{
			updateWord += "_ ";
		}	
		
		if(letter2){
			updateWord += word.charAt(2);
		}
		else{
			updateWord += "_ ";
		}	
		
		if(letter3){
			updateWord += word.charAt(3);
		}
		else{
			updateWord += "_ ";
		}	
		
		System.out.println("Your result is " + updateWord); //Print result

	}
	public static void runGame(String word){ //takes input

		boolean[] letters = new boolean [4];
		int numberOfGuess = 0;
		
		while(numberOfGuess < 6 && (letters[0] == false || letters[1] == false || letters[2] == false || letters[3] == false)){ 
			
			Scanner reader = new Scanner(System.in);// user input
			System.out.println("Please enter a letter");
			char find = reader.nextLine().charAt(0);
			find = toUpperCase(find); // convert to Uppercase
			
			if(isLetterInWord(word, find) == 0){
				letters[0] = true;	
			}
			
			if(isLetterInWord(word, find) == 1){
				letters[1] = true;
			}
			if(isLetterInWord(word, find) == 2){
				letters[2] = true;
			}
			 if(isLetterInWord(word, find) == 3){
				letters[3] = true;
			}	
				
			 if(isLetterInWord(word, find) == -1){
				numberOfGuess++;
			}	
			printWork(word, letters[0], letters[1], letters[2], letters[3]); 
			
		}	
		if(numberOfGuess >= 6){ //exceed the number of attempts
			System.out.println("Good effort, but you lost!");
		}else{
            System.out.println("Wow! You guessed the correct word: " + word);
		}
	}	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Please guess a word of your choice: ");
		String word = reader.next();
		word = word.toUpperCase();
		runGame(word);
		
	}

}