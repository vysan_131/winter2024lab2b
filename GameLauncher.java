import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello! Would you like to play Wordle or Hangman? Press 1 for Hangman and Press 2 for Wordle");
		int num = reader.nextInt();
		// if the user puts 2, he will play Wordle, so we're calling a Wordle method.
		if(num == 2 ){
			System.out.println("Here's Wordle: ");
			wordle();
				
		}
		// if the user puts 1, he will play Hangman, so we're calling a Hangman method.
		else if(num == 1){
			System.out.println("Here's Hangman: ");
			hangman();
		}
		// if the user doesn't enters 1 or 2, he'll be redoing the flow.
		else{
			System.out.println("Try again!");
		}	
		
		

	}
	//call to the Hangman class
	public static void hangman(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Please guess a word of your choice: ");
		String word = reader.next();
		word = word.toUpperCase();
		Hangman.runGame(word);
		
	}
	//call to the Wordle class
	public static void wordle(){
		
		String answer = Wordle.generateWord();
		Wordle.runGame(answer);
	} 
	
}	